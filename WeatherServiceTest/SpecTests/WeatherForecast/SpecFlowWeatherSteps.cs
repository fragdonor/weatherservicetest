﻿using RestSharp;
using System;
using TechTalk.SpecFlow;
using Xunit;
using WeatherServiceTest.Models;
using Newtonsoft.Json;

namespace WeatherServiceTest
{
    [Binding]
    public class SpecFlowWeatherSteps
    {
        // please change the API key to working one
        public const string _api = "0ac9c80abc379d8f4ba176f4429092af";
        public static int _cityDataIndex = -1;
        public const int numberOfCities = 1;
        public static Cities[] citiesData = new Cities[numberOfCities];
        
        public static string _dayToCheckWeather = String.Empty;
        public static string ServiceUrl = @"http://api.openweathermap.org";
        public static string _responseContent= String.Empty;
        public static WeatherForecastResponse _weatherResponse;

        [Given(@"I have cities data")]
        public void GivenIHaveCitiesData()
        {
            citiesData[0].id =  "6619279";
            citiesData[0].name =  "City of Sydney";
            citiesData[0].country =  "AU";
            citiesData[0].lon =  "151.208435";
            citiesData[0].lan =  "-33.867779";
        }
                
     

        [Given(@"I like to holiday in (.*)")]
        public void GivenILikeToHolidayIn(string city)
        {
            for (int i = 0;i<numberOfCities; i++)
            {
                if (citiesData[i].name.Contains(city))
                {
                    _cityDataIndex = i;
                    break;
                }
            }
        }
        [Given(@"I like to make holiday in the city with id (.*)")]
        public void GivenILikeToHolidayInTheCityWithId(string id)
        {
            for (int i = 0; i < numberOfCities; i++)
            {
                if (citiesData[i].id.Equals(id))
                {
                    _cityDataIndex = i;
                    break;
                }
            }
        }
        [Given(@"I like to make holiday in the city with langitude (.*) and longitude (.*)")]
        public void GivenILikeToMakeHolidayInTheCityWithLangitudeAndLongitude(long langitude, long longitude)
        {
            for (int i = 0; i < numberOfCities; i++)
            {
                if (citiesData[i].lon.Equals(longitude)&& citiesData[i].lan.Equals(langitude))
                {
                    _cityDataIndex = i;
                    break;
                }
            }
        }

        [When(@"I look up the weather forecast for these coordinates")]
        public void WhenILookUpTheWeatherForecastForTheseCoordinates()
        {
            var resource = @"/data/2.5/forecast/daily?lat=" + citiesData[_cityDataIndex].lan + "&lon=" + citiesData[_cityDataIndex].lon
                    + @"&units=metric&cnt=7&APPID="+ _api;
            SetResponseContent(resource);
        }
   
        [Given(@"I only like to holiday on next (.*)")]
        public void GivenIOnlyLikeToHolidayOnNext(string holiday)
        {
            _dayToCheckWeather=holiday;
        }
        
        [When(@"I look up the weather forecast for this city")]
        public void WhenILookUpTheWeatherForecastForThisCity()
        {
            var resource = @"/data/2.5/forecast/daily?q=" + citiesData[_cityDataIndex].name + "," + citiesData[_cityDataIndex].country
                    + @"&units=metric&cnt=7&APPID=" + _api;
            SetResponseContent(resource);
        }

       

        [When(@"I look up the weather forecast for this geoname_id")]
        public void WhenILookUpTheWeatherForecastForThisId()
        {
            var resource = @"/data/2.5/forecast/daily?id=" + citiesData[_cityDataIndex].id
                + @"&units=metric&cnt=7&APPID=" + _api;
            SetResponseContent(resource);
        }


        [Then(@"I receive the weather forecast for (.*)")]
        public void ThenIReceiveTheWeatherForecast(string city)
        {
            if (_responseContent.Contains("Invalid API key"))
                Assert.False(true, "Api key is invalid");
            else
            {
                _weatherResponse = JsonConvert.DeserializeObject<WeatherForecastResponse>(_responseContent);
                Assert.Contains(city, _weatherResponse.city.name);
                Assert.NotEqual(_responseContent, String.Empty);
                Assert.NotEqual(_responseContent, @"Invalid API key. Please see http://openweathermap.org/faq#error401 for more info.");
            }
        }
        [Then(@"the geoname_id in response is (.*)")]
        public void ThenTheCityIdInResponseIs(int p0)
        {
            Assert.Equal(p0, _weatherResponse.city.geoname_id);
        }


        [Then(@"the day temperature is warmer than (.*) degrees")]
        public void ThenTheDayTemperatureIsWarmerThanDegrees(int p0)
        {
            foreach (List day in _weatherResponse.list)
            {
                if (GetDayTimeForUnixTimeFormat(day.dt) == (DayOfWeek)Enum.Parse(typeof(DayOfWeek), _dayToCheckWeather))
                {
                    Assert.True(day.temp.day > p0, "Checking day temperature in response");
                    break;
                }
            }
            ClearData();
        }
        #region Common Functions
        public void SetResponseContent(string resourseParam)
        {
            if (_cityDataIndex != -1)
            {
                var client = new RestClient(ServiceUrl);
                var request = new RestRequest(Method.GET);
                var resource = resourseParam;

                request.Resource = resource;
                IRestResponse response = client.Execute(request);
                _responseContent = response.Content;
            }
        }
        public static void ClearData()
        {
            _cityDataIndex = -1;
            citiesData = new Cities[numberOfCities];
            _dayToCheckWeather = String.Empty;
            _responseContent = String.Empty;
            _weatherResponse = new WeatherForecastResponse();
        }
        public static DayOfWeek GetDayTimeForUnixTimeFormat(long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var daytime = epoch.AddSeconds(unixTime);
            return daytime.DayOfWeek;
        }

        #endregion
    }
}
