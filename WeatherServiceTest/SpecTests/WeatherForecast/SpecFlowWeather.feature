﻿Feature: SpecFlowWeather
	In order to get a weather forecast user calls a weather forecast service with a city as parameter
	And gets temperatures for 16 days

Background: 
Given I have cities data

Scenario: A happy holiday maker checks weather with city name
               Given I like to holiday in Sydney
               And I only like to holiday on next Thursday
               When I look up the weather forecast for this city
Then I receive the weather forecast for Sydney
And the day temperature is warmer than 10 degrees

Scenario: A happy holiday maker checks weather with geoname_id
               Given I like to make holiday in the city with id 6619279 
               And I only like to holiday on next Thursday
               When I look up the weather forecast for this geoname_id
Then I receive the weather forecast for Sydney
And  the geoname_id in response is 6619279
And the day temperature is warmer than 10 degrees

Scenario: A happy holiday maker checks weather with langitude and longitude
               Given I like to make holiday in the city with langitude -33.867779 and longitude 151.208435
               And I only like to holiday on next Thursday
               When I look up the weather forecast for these coordinates
Then I receive the weather forecast for Sydney
And  the geoname_id in response is 6619279
And the day temperature is warmer than 10 degrees

